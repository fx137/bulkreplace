﻿using BulkReplace.Helpers;
using BulkReplace.Models;
using BulkReplace.MVVMHelpers;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Input;

namespace BulkReplace.ViewModels
{
    class MainWindowViewModel : INotifyPropertyChanged
    {
        #region Private Fields

        private ReplacePattern _replacePattern;

        private bool _enableRegex;
        private string _textIn;
        private string _textOut;
        private string _messageText;
        private ICommand _loadFile;
        private ICommand _saveFile;
        private ICommand _copyOriginal;
        private ICommand _getInformation;

        private ISerializer<ReplacePattern> _serializer;

        #endregion

        #region Public Properties

        public string StrReplaces
        {
            get
            {
                return _replacePattern.Replaces.ToBreakedString();
            }

            set
            {
                _replacePattern.Replaces = value.SplitAtNewLine();
                ReplaceInText();
            }
        }
        public string StrOriginals
        {
            get
            {
                return _replacePattern.Originals.ToBreakedString();
            }

            set
            {
                _replacePattern.Originals = value.SplitAtNewLine();
                ReplaceInText();
            }
        }
        public string StrTextIn
        {
            get
            {
                return _textIn;
            }

            set
            {
                _textIn = value;
                ReplaceInText();
            }
        }
        public string StrTextOut
        {
            get
            {
                return _textOut;
            }
            private set
            {
                _textOut = value;
                OnPropertyChanged("StrTextOut");
            }
        }
        public string MessageText
        {
            get
            {
                return _messageText;
            }
            private set { }
        }

        public ICommand LoadFile
        {
            get
            {
                return _loadFile;
            }
            private set { }
        }
        public ICommand SaveFile
        {
            get
            {
                return _saveFile;
            }
            private set { }
        }
        public ICommand GetInformation
        {
            get
            {
                return _getInformation;
            }
            private set { }
        }
        public ICommand CopyOriginal
        {
            get
            {
                return _copyOriginal;
            }
            private set { }
        }

        public bool EnableRegex
        {
            get
            {
                return _enableRegex;
            }
            set
            {
                _enableRegex = value;
                OnPropertyChanged("EnableRegex");
            }
        }

        #endregion

        public MainWindowViewModel()
        {
            _replacePattern = new ReplacePattern();
            _textIn = "";
            _textOut = "";
            _messageText = "";

            _serializer = new FxXmlSerializer<ReplacePattern>();

            _saveFile = new RelayCommand(p => SavePatternToFile());
            _loadFile = new RelayCommand(p => LoadPatternFromFile());
            _copyOriginal = new RelayCommand(p => CopyOriginalsToReplaces());
            _getInformation = new RelayCommand(p => GetInformationAbout());
        }


        private void LoadPatternFromFile()
        {
            var odf = new OpenFileDialog()
            {
                DefaultExt = "fxRplc",
                Filter = "Fx-Replace Presets (*.fxRplc)|*.fxRplc"
            };
            if (odf.ShowDialog() == true)
            {
                try
                {
                    _replacePattern = _serializer.DeserializeFromFile(odf.FileName);
                    OnPropertyChanged("StrOriginals");
                    OnPropertyChanged("StrReplaces");
                }
                catch (InvalidOperationException)
                {
                    MessageBox.Show("Error Parsing, file might be corrupt or invalid");
                }
            }
        }

        private void SavePatternToFile()
        {
            var sfd = new SaveFileDialog()
            {
                DefaultExt = "fxRplc",
                Filter = "Fx-Replace Presets (*.fxRplc)|*.fxRplc"
            };
            if (sfd.ShowDialog() == true)
            {
                _serializer.SerializeToFile(_replacePattern, sfd.FileName);
            }
        }

        private void CopyOriginalsToReplaces()
        {
            StrReplaces = StrOriginals;
            OnPropertyChanged("StrReplaces");
        }

        private void ReplaceInText()
        {
            string replaceText = _textIn;

            List<string> originalsList = _replacePattern.Originals;
            List<string> replacesList = _replacePattern.Replaces;

            if (originalsList.Count == replacesList.Count && originalsList.Count > 0)
            {
                if (!_enableRegex)
                {
                    for (int i = 0; i < originalsList.Count; i++)
                    {
                        if (originalsList[i].Length > 0 && replacesList[i].Length > 0)
                        {
                            replaceText = replaceText.Replace(originalsList[i], replacesList[i]);
                        }
                    }
                }
                else
                {
                    try
                    {
                        for (int i = 0; i < originalsList.Count; i++)
                        {
                            if (originalsList[i].Length > 0 && replacesList[i].Length > 0)
                            {
                                replaceText = Regex.Replace(replaceText, originalsList[i], replacesList[i]);
                            }
                        }
                    }
                    catch (ArgumentException)
                    {
                        _messageText = "Invalid argument, revise your Regex";
                        OnPropertyChanged("MessageText");
                    }
                }
                _textOut = replaceText;
                OnPropertyChanged("StrTextOut");
            }
        }

        private void GetInformationAbout()
        {
            MessageBox.Show("Copyright (c) 2017 Fx\r\n " +
                "Permission is hereby granted, free of charge, to any person obtaining a " +
                "copy of this software and associated documentation files(the \"Software\"), " +
                "to deal in the Software without restriction, including without limitation the " +
                "rights to use, copy, modify, merge, publish, distribute, sublicense, and / or " +
                "sell copies of the Software, and to permit persons to whom the Software is " +
                "furnished to do so, subject to the following conditions:\r\n" +
                "The above copyright notice and this permission notice shall be included" +
                "in all copies or substantial portions of the Software.\r\n" +
                "THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR " +
                "IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, " +
                "FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE " +
                "AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, " +
                "WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR " +
                "IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.");
        }

        #region Property Changed Implementation
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
        #endregion
    }
}
