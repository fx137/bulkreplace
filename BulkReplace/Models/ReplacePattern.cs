﻿using System;
using System.Collections.Generic;

namespace BulkReplace.Models
{
    [Serializable]
    public class ReplacePattern
    {
        private List<string> _originals;
        private List<string> _replaces;

        public ReplacePattern()
        {
            _originals = new List<string>();
            _replaces = new List<string>();
        }


        /// <summary>
        /// List of original String
        /// </summary>
        public List<string> Originals
        {
            get
            {
                return _originals;
            }
            set
            {
                _originals = value;
            }
        }
        /// <summary>
        /// List of replacement strings
        /// </summary>
        public List<string> Replaces
        {
            get
            {
                return _replaces;
            }
            set
            {
                _replaces = value;
            }
        }
    }
}
