﻿using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BulkReplace.Helpers
{
    public static class FxHelpers
    {
        public static string NormalizeCRLF(this string input)
        {
            input = input.Replace("\r\n", "\r");
            input = input.Replace("\r", "\n");
            return input.Replace("\n", "\r\n");
        }

        public static List<string> SplitAtNewLine(this string input)
        {
            input = input.Replace("\r\n", "\r");
            input = input.Replace("\r", "\n");

            return input.Split('\n').ToList();
        }

        public static List<string> RemoveEmptyStrings(this List<string> input)
        {
            var itemsToRemove = new List<int>();

            for (int i = 0; i < input.Count; i++)
            {
                if (string.IsNullOrWhiteSpace(input[i]) || string.IsNullOrEmpty(input[i]))
                {
                    itemsToRemove.Add(i);
                }
            }
            for (int i = itemsToRemove.Count - 1; i >= 0; i--)
            {
                input.RemoveAt(i);
            }
            return input;
        }

        public static string ToBreakedString(this List<string> input)
        {
            var sb = new StringBuilder();

            if (input.Count > 0)
            {
                for (int i = 0; i < input.Count - 1; i++)
                {
                    sb.AppendLine(input[i]);
                }
                sb.Append(input[input.Count - 1]);
            }
            return sb.ToString();
        }
    }
}
