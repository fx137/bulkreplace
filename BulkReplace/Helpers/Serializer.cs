﻿using System.IO;
using System.Xml.Serialization;

namespace BulkReplace.Helpers
{
    public interface ISerializer<T>
    {
        void SerializeToStream(T item, ref Stream dataStream);
        void SerializeToFile(T item, string fileName);
        T DeserializeFromStream(ref Stream dataStream);
        T DeserializeFromFile(string fileName);
    }

    public class FxXmlSerializer<T> : ISerializer<T>
    {
        private XmlSerializer dataSerializer;

        public FxXmlSerializer()
        {
            dataSerializer = new XmlSerializer(typeof(T));
        }

        public void SerializeToStream(T item, ref Stream dataStream)
        {
            dataSerializer.Serialize(dataStream, item);
        }

        public T DeserializeFromStream(ref Stream dataStream)
        {
            return (T)dataSerializer.Deserialize(dataStream);
        }

        public void SerializeToFile(T item, string fileName)
        {
            Stream fs = new FileStream(fileName, FileMode.Create, FileAccess.ReadWrite);
            SerializeToStream(item, ref fs);
            fs.Dispose();
        }
        public T DeserializeFromFile(string fileName)
        {
            Stream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read);
            T item = DeserializeFromStream(ref fs);
            fs.Dispose();
            return item;
        }
    }
}
